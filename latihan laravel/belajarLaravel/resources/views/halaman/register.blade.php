<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label> First name:</label>
        <input type="text" name="fname" > <br> <br>
        <label> Last name:</label>
        <input type="text" name="lname" > <br> <br>
        <label >Gender:</label><br>
        <input type="radio">Male <br>
        <input type="radio">Female <br>
        <input type="radio">Other <br>
        <label >Nationality:</label><br> 
        <select name="Nationality">
        <option value="indonesian">indonesian</option>
        <option value="singapore">singapore</option>
        <option value="malaysian">malaysian</option>
        <option value="australian">australian</option>
        </select> <br><br>
        <label>Language Spoken</label> <br>
        <input type="checkbox" name="Language Spoken">Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken">English <br>
        <input type="checkbox" name="Language Spoken">Other <br>
        <label>Bio:</label>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea> <br><br>
        <button type="submit" >sign up</button>
    </form>
</body>
</html>