<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php
    echo "<h3>Soal 1 </h3>";

    echo "LOOPING PERTAMA <br>";
    $i = 2;
    do{
        echo " $i - I Love PHP <br>";
        $i+=2;
    }while($i <= 20);

    echo "LOOPING KEDUA <br>";
    for($a=20; $a>=2; $a-=2){
        echo " $a - I Love PHP <br>";
    }

    echo "<h3>Soal 2 </h3>";

    $numbers = [18, 45, 29, 61, 47, 34];
    echo "array numbers: ";
    print_r($numbers);
    echo "<br>";

    foreach($numbers as $nilai){
        $rest[] = $nilai %=5;
    }

    echo "Array sisa baginya adalah : ";

    print_r($rest);
    echo "<br>";

    echo "<h3>Soal 3 </h3>";

    $items = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach($items as $indexBio){
        $tampung = [
            "id" => $indexBio[0],
            "nama" => $indexBio[1],
            "price" => $indexBio[2],
            "description" => $indexBio[3],
            "source" => $indexBio[4],
        ];
        print_r($tampung);
        echo "<br>";
        }

        echo "<h3>Soal 4 </h3>";

        $l=5;
        for($k=$l; $k>=1; $k--){
            for($i=$l; $i>=$k; $i--){
            echo "*";
            }
            echo "<br>";
        }
            

    ?>

</body>
</html>