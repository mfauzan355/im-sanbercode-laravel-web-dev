<?php 
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$Animal = new Animal("shaun");

echo "name : " . $Animal->name . "<br>"; // "shaun"
echo "legs : " . $Animal->legs . "<br>"; // 4
echo "cold blooded : " . $Animal->cold_blooded . "<br> <br>" ; // "no"

$kodok = new Frog ("buduk");

echo "name : " . $kodok->name . "<br>"; 
echo "legs : " . $kodok->legs . "<br>"; 
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo $kodok->jump() . "<br> <br>";

$Ape = new Ape ("kera sakti");

echo "name : " . $Ape->name . "<br>"; 
echo "legs : " . $Ape->legs . "<br>"; 
echo "cold blooded : " . $Ape->cold_blooded . "<br>";
echo "yell : " . $Ape->yell . "<br>";


?>
